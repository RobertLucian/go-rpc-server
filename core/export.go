package core

import (
	"image"

	"github.com/disintegration/imaging"
)

type JpegImage struct {
	Img image.YCbCr
}

type Gray struct {
	Img image.Gray
}

type NRGBAImage struct {
	Img image.NRGBA
}

type Preprocess int

func (t *Preprocess) ConvertToGrayscale(src *JpegImage, out *Gray) error {
	var (
		bounds = src.Img.Bounds()
		gray   = image.NewGray(bounds)
	)
	for x := 0; x < bounds.Max.X; x++ {
		for y := 0; y < bounds.Max.Y; y++ {
			var rgba = src.Img.At(x, y)
			gray.Set(x, y, rgba)
		}
	}
	out.Img = *gray

	return nil
}

func (t *Preprocess) BlurImage(src *image.Image, out *NRGBAImage) error {

	out.Img = *imaging.Blur(*src, 0.5)

	return nil
}
