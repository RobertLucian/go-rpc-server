package main

import (
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"log"
	"net/rpc"
	"os"
	"path/filepath"
	"sync"

	"./core"
)

func processImage(img image.Image) image.Gray {
	gray := *rgbaToGray(img)
	return gray
}

func rgbaToGray(img image.Image) *image.Gray {
	var (
		bounds = img.Bounds()
		gray   = image.NewGray(bounds)
	)
	for x := 0; x < bounds.Max.X; x++ {
		for y := 0; y < bounds.Max.Y; y++ {
			var rgba = img.At(x, y)
			gray.Set(x, y, rgba)
		}
	}
	return gray
}

func main() {
	var files []string
	var wg sync.WaitGroup

	// gob.Register(&image.YCbCr{})

	root := "./dataset"
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() == false {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}

	wg.Add(len(files))

	client, err := rpc.DialHTTP("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("dialing:", err)
	}

	for idx, file := range files {
		go func(idx int, file string) {
			defer wg.Done()

			// read random image
			infile, err := os.Open(file)
			if err != nil {
				panic(err.Error())
			}
			defer infile.Close()
			log.Println("read image", idx)

			// decode image
			src, err := jpeg.Decode(infile)
			if err != nil {
				panic(err.Error())
			}
			log.Println("decode image", idx)

			// process image
			// gray := processImage(src)
			if img, ok := src.(*image.YCbCr); ok {
				log.Println("send image", idx, " to convert to grayscale")
				imgg := core.JpegImage{*img}
				var gray core.Gray
				err = client.Call("Preprocess.ConvertToGrayscale", imgg, &gray)
				if err != nil {
					log.Fatal(err.Error(), ", on image with index ", idx)
				}
				log.Println("image", idx, " converted to grayscale")

				// save image
				outfile, _ := os.Create(fmt.Sprintf("./converted/%d.png", idx))
				png.Encode(outfile, &gray.Img)
				outfile.Close()
				log.Println("image", idx, " saved to file")
			}

		}(idx, file)
	}

	wg.Wait()
}
