package main

import (
	"log"
	"net"
	"net/http"
	"net/rpc"

	"./core"
)

func main() {
	arith := new(core.Preprocess)
	rpc.Register(arith)
	rpc.HandleHTTP()

	listen, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal("dialing:", err)
	}

	http.Serve(listen, nil)
}
