# go-rpc-server

## Tema

Sistem client/server care sa foloseasca apeluri RPC.

## Solutie

Sistem bazat pe apeluri RPC (Remote Procedure Call), unde clientul trimite o lista de
imagini in format color, server-ul le converteste in alb si negru, le trimite inapoi, iar in final
clientul le salveaza intr-un director separat.

## Detalii implementare

Exista 3 fisiere care contin codul sursa:

* `core/export.go` - fisier care contine implementarea functilor ce sunt apelate remote de client si publicate de server.
* `server.go` - server care public functiile din `core/export.go` pentru toti clientii.
* `client.go` - client care se conecteaza la server pentru a apela functiile publicate de server.

Exista 2 functii publicate de server:

* `ConvertToGrayscale(src *core.JpegImage, out *core.Gray)` aflata in namespace-ul `Preprocess` care ia o imagine color in format JPEG si o face alb-negru si returneaza prin parametrul `out`.
* `BlurImage(src *image.Image, out *image.NRGBA)` aflata in namespace-ul `Preprocess` care o imagine in orice format si returneaza o imagine blurata in RGBA.

Clientul cand porneste, citeste toate numele de fisiere din directorul `dataset` (`dataset`-ul trebuie sa contina numai imagini), se conecteaza la serverul de RPC pornit pe localhost pe portul `1234` si apoi genereaza cate o rutina go pe fiecare imagine, unde imaginea e citita, decodata, si apoi trimisa ca parametru catre functia publicata de server numita `Preprocess.ConvertToGrayscale` unde aceasta e convertita in alb si negru. Rezultatul e returnat si apoi este scrisa intr-un fisier al carui nume e un index in directorul `converted`.

## Concluzii

De indata ce pentru fiecare imagine din directorul `dataset` se instantiaza o rutina go, se poate zice ca client-ul nu va fi incetinit de operatiile IO care sunt de altfel groaznic de incete. Performanta ajunge sa se justifice la un numar mai mare de imagini (ordinul miilor) si odata cu prezenta mai multor clienti. 

De altfel, convertirea unei imagini la alb-negru este una, dar sistemul poate fi impins spre a face operatii specifice AI-ul, unde se cunoaste ca e nevoie de suficienta putere de procesare. In general, RPC-ul este folosit acolo unde coupling dintre client si ce ofera server-ul este mare, deci probabil ca o astfel de implementare este facuta acolo unde conexiunea dintre server si client e stabila si puternica si atunci cand serviciul e oferit intern (in cadrul unei organizatii spre exemplu).